var express = require('express');
var router = express.Router();
var app = require("../app.js");
/* GET articles page. */
router.get('/', function(req, res, next) {
  const query = `SELECT * FROM articles`;
  var articles = [];
  var db = app.getDb();
  db.all(query, [], (err, rows) => {
      if (err)
      {
          console.error(err.message);
      }
      else{
        articles = rows;
      }
  res.render('articles', { title: 'Les articles de mon blog', articles: articles });
 });
});

/* GET article page. */
router.get('/:id/:route', function(req, res, next) {
  const query = `SELECT * FROM articles where id = ${req.params.id}`;
  var article = [];
  var db = app.getDb();
  db.all(query, [], (err, rows) => {
      if (err)
      {
          console.error(err.message);
      }
      else{
        article = rows[0];
      }
  res.render('article', { title: article.title, article: article });
 });
});

/* GET ajouter un article - page. */

// Pour afficher la page
router.get('/ajouter-article', function(req, res, next) {
  res.render('ajouterarticle', { title: 'Ajouter un article' });
});
// Pour ajouter les inputs du form dans la base de données
router.post('/ajouter-article', function(req, res, next) {
  const query = `INSERT INTO articles (title, content, author_id) VALUES ('${req.body.titre}','${req.body.contenu}',1)`;
  var db = app.getDb();
  db.run(query,function (err) {
    if (err) {
      console.log(err)
    } else {
      res.send('ajouterarticle-resultat')
    }
  })
  res.render('ajouterarticle-resultat', { state: true });
});



module.exports = router;

   