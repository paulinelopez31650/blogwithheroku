var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const port = process.env.PORT || 8080;

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var articlesRouter = require('./routes/articles');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/articles', articlesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// SQLite
const sqlite3 = require('sqlite3').verbose();

// open database
var db = new sqlite3.Database('blogheroku.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err)
    {
        return console.error(err.message);
    }
    console.log('Connected to the Grocery SQlite database.');
});
/**
 * Listen for connections
 */
app.listen(port, () => {
  console.log(`Blog app listening at http://localhost:${port}`)
});

getDb = () => {
  return db;
}

exports.app = app;
exports.getDb = getDb;
